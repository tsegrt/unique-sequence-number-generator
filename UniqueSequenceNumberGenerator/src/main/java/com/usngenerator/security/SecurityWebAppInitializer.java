package com.usngenerator.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Security web application initializer class.
 * 
 * @author Tomislav Segrt
 */
public class SecurityWebAppInitializer extends AbstractSecurityWebApplicationInitializer {

}
