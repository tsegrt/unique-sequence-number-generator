package com.usngenerator.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usngenerator.model.Account;
import com.usngenerator.service.AccountService;


/**
 * Service for providing user information which are also used for security purposes.
 * 
 * @author Tomislav Segrt
 */
@Service
public class UsnGeneratorUserDetailsService implements UserDetailsService {

	@Autowired
	private AccountService accountService;
	
	@Transactional(readOnly=true)
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountService.findByUsername(username);
		if(account == null) {
			throw new UsernameNotFoundException("Username not found!");
		}
		return new User(account.getUsername(), account.getPassword(), true, true, true, true, getGrantedAuthorities(account));
	}
	
	private List<GrantedAuthority> getGrantedAuthorities(Account account) {
		List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		return grantedAuthorities;
	}

}
