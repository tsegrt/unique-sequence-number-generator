package com.usngenerator;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;

/**
 * Main application class, also used for getting and listing of sorted beans.
 * 
 * @author Tomislav Segrt
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer{
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);
		
		String[] beans = context.getBeanDefinitionNames();
		Arrays.sort(beans);
		for(String beanName : beans) {
			System.out.println(beanName);
		}
	}

}
