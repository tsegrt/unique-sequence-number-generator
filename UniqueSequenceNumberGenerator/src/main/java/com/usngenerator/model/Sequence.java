package com.usngenerator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.usngenerator.utils.DateTimeSerializer;


/**
 * Represents an unique serial number generated during the allocation for some document.
 * 
 * @author Tomislav Segrt
 */
@Entity
public class Sequence {
	
	/**
	 * An unique identifier.
	 */
	private int id;
	
	/**
	 * The lowest available sequence number.
	 */
	private long sequence;
	
	/**
	 * Current date and time of number generation.
	 */
	private DateTime date;
	
	/**
	 * User who used this sequence number.
	 */
	private Account account;
	
	/**
	 * Short description of sequence purpose.
	 */
	private String purpose;

	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	@Min(1)
	@Column(nullable=false, unique=true)
	public long getSequence() {
		return sequence;
	}
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
	
	
	@JsonSerialize(using = DateTimeSerializer.class)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(nullable=false)
	public DateTime getDate() {
		return date;
	}
	
	public void setDate(DateTime date) {
		this.date = date;
	}

	
	@Size(min=1)
	@Column(nullable=false)
	public String getPurpose() {
		return purpose;
	}
	
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	
	
	@ManyToOne
	@JoinColumn(name="account_id", nullable=false)
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}
	

}
