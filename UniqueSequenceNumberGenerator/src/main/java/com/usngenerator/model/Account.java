package com.usngenerator.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents an user account with login credentials. 
 * An user can have many generated sequences.
 * 
 * @author Tomislav Segrt
 */
@Entity
public class Account {

	/**
	 * An unique identifier.
	 */
	private int id;
	
	/**
	 * An unique username of this user for logging in application.
	 */
	private String username;
	
	/**
	 * Encrypted password of this user for logging in application.
	 */
	private String password;
	
	/**
	 * Sequences generated on request by this user.
	 */
	private Set<Sequence> sequences = new HashSet<Sequence>();
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	@Column(nullable=false, unique=true)
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	@JsonIgnore
	@Column(nullable=false)
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	
	@JsonIgnore
	@OneToMany(mappedBy="account")
	public Set<Sequence> getSequences() {
		return sequences;
	}

	public void setSequences(Set<Sequence> sequences) {
		this.sequences = sequences;
	}

}
