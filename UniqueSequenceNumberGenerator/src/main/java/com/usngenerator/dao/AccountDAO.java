package com.usngenerator.dao;

import com.usngenerator.model.Account;


/**
 * An user account domain interface with read method for implementation.
 * 
 * @author Tomislav Segrt
 */
public interface AccountDAO {

	public Account findByUsername(String username);
	
}
