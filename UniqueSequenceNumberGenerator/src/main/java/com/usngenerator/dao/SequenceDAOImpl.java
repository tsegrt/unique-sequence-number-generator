package com.usngenerator.dao;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.usngenerator.model.Account;
import com.usngenerator.model.Sequence;


/**
 * A sequence repository with the implementation of create and read methods.
 * 
 * @author Tomislav Segrt
 */
@Repository
public class SequenceDAOImpl implements SequenceDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private AccountDAO accountDAO;

	/**
	 * Gets a list of all generated sequences.
	 * 
	 * @return 	list of all generated sequences
	 */
	@Override
	public List<Sequence> getSequences() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Sequence.class);
		@SuppressWarnings("unchecked")
		List<Sequence> sequenceList = (List<Sequence>) criteria.list();
		return sequenceList;
	}

	
	/**
	 * Adds a new sequence with given user account, short description and next available number.
	 * 
	 * @param username	an unique identifier for user account
	 * @param purpose	short description of new sequence
	 * @param number	the lowest available number
	 * @return 			a new generated unique serial number(sequence)
	 */
	@Override
	public Sequence addSequence(String username, String purpose, long number) {
		Sequence sequence = new Sequence();
		sequence.setSequence(number);
		Account account = accountDAO.findByUsername(username);
		sequence.setAccount(account);
		sequence.setPurpose(purpose);
		sequence.setDate(DateTime.now(DateTimeZone.UTC));
		sessionFactory.getCurrentSession().saveOrUpdate(sequence);
		return sequence;
	}

	/**
	 * Gets a sequence with the given value.
	 * 
	 * @param number	a number of required sequence
	 * @return			a corresponding sequence
	 */
	@Override
	public Sequence readSequence(long number) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Sequence.class)
															  .add(Restrictions.eq("sequence", number));
		Sequence sequence = (Sequence) criteria.uniqueResult();
		return sequence;
	}

	/**
	 * Gets the next unique serial number based on currently generated sequences.
	 * If difference between consecutive sequences from list is greater then 1, then next available number is value between them.
	 * Otherwise the result is equal to next number after the highest value from this list. 
	 * 
	 * @return 	the lowest available unique serial number
	 */
	@Override
	public Long findSequence() {
		String hql = "select seq.sequence from Sequence seq order by seq.sequence asc";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Long> results = (List<Long>) query.list();
		long result = 0;
		if(!results.isEmpty()) {
			result = results.get(results.size()-1);
			for(int i=1; i<results.size(); i++) {
				if(results.get(i) - results.get(i-1) > 1) {
					result = results.get(i-1);
					break;
				} 
			}
		}	
		AtomicLong number = new AtomicLong(result);
		return number.incrementAndGet();
	}

}
