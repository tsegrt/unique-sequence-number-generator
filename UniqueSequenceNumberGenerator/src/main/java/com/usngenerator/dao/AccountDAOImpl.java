package com.usngenerator.dao;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.usngenerator.model.Account;


/**
 * An user account repository with implemented read method.
 * 
 * @author Tomislav Segrt
 */
@Repository
public class AccountDAOImpl implements AccountDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	/**
	 * Finds a corresponding user account with the given username.
	 * 
	 * @param username	an unique username 
	 * @return			an user account with the corresponding username
	 */
	@Override
	public Account findByUsername(String username) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Account.class)
				  .add(Restrictions.eq("username", username));
		Account account = (Account) criteria.uniqueResult();
		return account;
	}

}
