package com.usngenerator.dao;

import java.util.List;

import com.usngenerator.model.Sequence;


/**
 * A sequence domain interface with the list of create and read methods for implementation.
 * 
 * @author Tomislav Segrt
 */
public interface SequenceDAO {

	public List<Sequence> getSequences();
	
	public Sequence addSequence(String username, String purpose, long number);
	
	public Sequence readSequence(long number);
	
	public Long findSequence();
	
}
