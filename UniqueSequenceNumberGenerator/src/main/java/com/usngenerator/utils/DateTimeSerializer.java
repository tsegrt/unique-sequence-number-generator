package com.usngenerator.utils;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;


/**
 * Serializer for customizing format of DateTime data types.
 * 
 * @author Tomislav Segrt
 */
public class DateTimeSerializer extends JsonSerializer<DateTime> {

	private static String first = "MMMM d'st' yyyy, hh:mm:ss a";
	private static String second = "MMMM d'nd' yyyy, hh:mm:ss a";
	private static String third = "MMMM d'rd' yyyy, hh:mm:ss a";
	private static String fourth = "MMMM d'th' yyyy, hh:mm:ss a";

	private static DateTimeFormatter formatter;

	@Override
	public void serialize(DateTime time, JsonGenerator generator, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		String pattern = getPattern(time.getDayOfMonth());
		formatter = DateTimeFormat.forPattern(pattern);
		generator.writeString(formatter.print(time));
	}
	
	/**
	 * Gets an appropriate pattern for DateTime format.
	 * 
	 * @param day	a given day of month
	 * @return		a corresponding pattern for given day
	 */
	private String getPattern(int day) {
		String pattern;
		switch(day) {
		case 1:
		case 21:
		case 31:
			pattern = first;
			break;
		case 2:
		case 22:
			pattern = second;
			break;
		case 3:
		case 23:
			pattern = third;
			break;
		default:
			pattern = fourth;
			break;
		}
		return pattern;
	}
	
	
	
}
