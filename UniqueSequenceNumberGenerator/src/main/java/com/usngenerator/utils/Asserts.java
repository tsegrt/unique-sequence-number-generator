package com.usngenerator.utils;


/**
 * Common argument checking methods that are used for exception handling in controller layer.
 * 
 * @author Tomislav Segrt
 */
public class Asserts {
	
	public static <T> void argumentIsNotNull(T arg, String argName) {
        if (arg == null)
            throw new IllegalArgumentException("Null argument " + argName + " passed!");
    }
	
	public static void argumentIsNotNullNorEmpty(String arg, String argName) {
        if (arg == null || arg.isEmpty())
            throw new IllegalArgumentException("Null argument " + argName + " passed!");
    }
	
	public static <T> void argumentHasNotChanged(T arg1, T arg2, String argName) {
		if (arg1 != arg2) {
			throw new IllegalArgumentException("Error, "+ argName + " has been changed, try again!");
		}
	}
	
	public static <T> void argumentsAreEqual(T arg1, T arg2, String argName) {
		if (!arg1.equals(arg2)) {
			throw new IllegalArgumentException("Error, wrong " + argName + "!");
		}
	}
	
}
