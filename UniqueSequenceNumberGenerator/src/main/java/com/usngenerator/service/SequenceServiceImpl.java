package com.usngenerator.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usngenerator.dao.SequenceDAO;
import com.usngenerator.model.Sequence;


/**
 * A sequence service with implemented transactional create and read methods from corresponding sequence repository.
 * 
 * @author Tomislav Segrt
 */
@Transactional
@Service
public class SequenceServiceImpl implements SequenceService {

	@Autowired
	private SequenceDAO sequenceDAO;
	
	@Override
	public List<Sequence> getSequences() {
		return sequenceDAO.getSequences();
	}

	@Override
	public Sequence addSequence(String username, String purpose, long number) {
		return sequenceDAO.addSequence(username, purpose, number);
	}

	@Override
	public Sequence readSequence(long number) {
		return sequenceDAO.readSequence(number);
	}

	@Override
	public Long findSequence() {
		return sequenceDAO.findSequence();
	}

}
