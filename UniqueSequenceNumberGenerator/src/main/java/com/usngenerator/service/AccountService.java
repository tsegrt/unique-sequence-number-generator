package com.usngenerator.service;

import com.usngenerator.model.Account;


/**
 * An user account domain interface with read method for implementation in service layer.
 * 
 * @author Tomislav Segrt
 */
public interface AccountService {

	public Account findByUsername(String username);
	
}
