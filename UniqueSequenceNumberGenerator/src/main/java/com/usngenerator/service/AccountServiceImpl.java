package com.usngenerator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usngenerator.dao.AccountDAO;
import com.usngenerator.model.Account;


/**
 * An user account service with implemented transactional read method from corresponding account repository.
 * 
 * @author Tomislav Segrt
 */
@Transactional
@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;
	
	@Override
	public Account findByUsername(String username) {
		return accountDAO.findByUsername(username);
	}

}
