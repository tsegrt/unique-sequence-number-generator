package com.usngenerator.service;

import java.util.List;

import com.usngenerator.model.Sequence;


/**
 * A sequence domain interface with list of create and read methods for implementation in service layer.
 * 
 * @author Tomislav Segrt
 */
public interface SequenceService {

	public List<Sequence> getSequences();
	
	public Sequence addSequence(String username, String purpose, long number);
	
	public Sequence readSequence(long number);
	
	public Long findSequence();
	
}
