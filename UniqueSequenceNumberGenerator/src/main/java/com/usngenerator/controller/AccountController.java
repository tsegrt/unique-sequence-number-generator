package com.usngenerator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.usngenerator.model.Account;
import com.usngenerator.service.AccountService;


/**
 * An account domain REST controller for handling web request that is sent for getting corresponding user account.
 * 
 * @author Tomislav Segrt
 */
@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;
	
	
	@RequestMapping(value = "/findByUsername", method = RequestMethod.GET)
	public Account findByUsername() {
		return accountService.findByUsername(getPrincipal());
	}
	
	/**
	 * Gets username of currently logged in user.
	 *  
	 * @return	username of current user
	 */
	public String getPrincipal(){
		String username = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(principal instanceof UserDetails) {

			username = ((UserDetails) principal).getUsername();
		}
		else {
			username = principal.toString();
		}
		return username;
	}
}
