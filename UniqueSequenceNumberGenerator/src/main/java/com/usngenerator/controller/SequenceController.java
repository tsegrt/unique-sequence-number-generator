package com.usngenerator.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usngenerator.model.Sequence;
import com.usngenerator.service.SequenceService;
import com.usngenerator.utils.Asserts;

/**
 * A sequence domain REST controller for handling web requests that are sent for creating of new sequence or getting a single entity or list of existing sequences.
 * 
 * @author Tomislav Segrt
 */
@RestController
public class SequenceController {
	
	@Autowired
	private SequenceService sequenceService;
	
	@Autowired 
	private AccountController accountController;

	@RequestMapping(value = "/getSequences", method = RequestMethod.GET)
	public List<Sequence> getSequences() {
		return sequenceService.getSequences();
	}
	
	@RequestMapping(value = "/addSequence", method = RequestMethod.POST)
	public Sequence addSequence(@RequestBody Map<String, Object> sequenceMap) {
		long number = Long.parseLong(sequenceMap.get("number").toString());
		Asserts.argumentIsNotNull(number, "sequence");
		Asserts.argumentHasNotChanged(number, findSequence(), "sequence");
		String username = sequenceMap.get("username").toString();
		Asserts.argumentIsNotNullNorEmpty(username, "username");
		String principal = accountController.getPrincipal();
		Asserts.argumentsAreEqual(username, principal, "user");
		String purpose = sequenceMap.get("purpose").toString();
		Asserts.argumentIsNotNullNorEmpty(purpose, "purpose");
		return sequenceService.addSequence(username, purpose, number);
	}
	
	@RequestMapping(value = "/readSequence", method = RequestMethod.GET)
	public Sequence readSequence(@RequestParam long number) {
		return sequenceService.readSequence(number);
	}
	
	@RequestMapping(value= "/findSequence", method = RequestMethod.GET)
	public long findSequence() {
		return sequenceService.findSequence();
	}
}
