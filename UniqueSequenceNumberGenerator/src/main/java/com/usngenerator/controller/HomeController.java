package com.usngenerator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Main controller for mapping views for login page and homepage to corresponding URL's.
 * 
 * @author Tomislav Segrt
 */
@Controller
public class HomeController {

	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public String loginPage() {
		return "login";
	}
	
	@RequestMapping(value = "/user/", method = RequestMethod.GET)
	public String homePage() {
		return "home";
	}
}
