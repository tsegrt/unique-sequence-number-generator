(function() {
	
	var app = angular.module("generator");
	
	var SequenceController = function($scope, $http, $location) {
		
		$scope.cancel = function () {
			$location.path("/");
		};
		
		
		var onUserFound = function(response) {
			$scope.user = response.data;
		};
		
		var onError = function(reason) {
			$scope.error = reason.data.message;
		};
		
		$http.get("/findByUsername").then(onUserFound, onError);
		
		
		var onNumberFound = function(response) {
			$scope.number = response.data;
		};
		
		$http.get("/findSequence").then(onNumberFound, onError);
		
		
		var onSequenceAdded = function(response) {
			$location.path("/");
		};
		
		var onSequenceError = function(reason) {
			$scope.error = reason.data.message;
			$http.get("/findSequence").then(onNumberFound);
		};
		
		$scope.addSequence = function(username, purpose) {
			$scope.sequence = {number: $scope.number, username: username, purpose: purpose};
			$http.post("/addSequence", $scope.sequence).then(onSequenceAdded, onSequenceError);
		};
		
		$scope.refresh = function(){
			$scope.error = false;
		};
	};
	
	app.controller("SequenceController", SequenceController);
	
}());