(function() {
	var app = angular.module("generator", ["ngRoute"]);
	
	app.config(function($routeProvider){
		$routeProvider.when("/", {
			templateUrl:"/static/templates/user.html",
			controller:"UserController"
		}).when("/sequence", {
			templateUrl:"/static/templates/sequence.html",
			controller:"SequenceController"
		}).otherwise({redirectTo:"/"});
	});
	
	app.filter("numberFixedLength", function() {
		return function(n, len) {
			var num = parseInt(n, 10);
			length = parseInt(len, 10);
			if (isNaN(num) || isNaN(len)) {
				return n;
			}
			num = '' + num; 
			while (num.length < len ) {
				num = '0' + num;	
			}
			return num;
		};
	});
	
	app.filter("pagination", function() {
		return function(input, start) {
			if(!input || !input.length) {
				return;
			}
			start = +start;
			return input.slice(start);
		};
	});
	
}());