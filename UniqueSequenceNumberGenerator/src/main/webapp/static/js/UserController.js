(function() {
	
	var app = angular.module("generator");
	
	var UserController = function($scope, $http, $location) {
		
		$scope.currentPage = 0;
		$scope.pageSize = 5;
		
		var onFound = function(response) {
			$scope.sequences = response.data;
			if($scope.sequences.length > 0) {
				$scope.numberOfPages = Math.ceil($scope.sequences.length / $scope.pageSize);
			}
			else {
				$scope.numberOfPages = 1;
			}
		};
		
		var onError = function(reason) {
			$scope.error = reason.data.message;
		}
		
		$http.get("/getSequences").then(onFound, onError);
		
		
		$scope.newSequence = function() {
			$location.path("/sequence");
		};
		
		
		var onRead = function(response) {
			$scope.sequence = response.data;
		};
		
		$scope.readSequence = function(sequence) {
			$http.get("/readSequence?number=" + sequence).then(onRead, onError)
		};
		
		
		$scope.search = function(filter, length) {
			$scope.searchFilter = filter;
			$scope.currentPage = 0;
			if(length > 0) {
				$scope.numberOfPages = Math.ceil(length / $scope.pageSize);
			}
			else {
				$scope.numberOfPages = 1;
			}
		};
		
	};
	
	app.controller("UserController", UserController);
	
}());

